build:
	docker-compose build

start:
	docker-compose up

up:
	docker-compose up -d

down:
	docker-compose down

fix-permissions:
	sudo chown -R esteban:esteban .

shell:
	docker-compose exec node /bin/ash 