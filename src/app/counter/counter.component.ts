import { Component, OnInit } from '@angular/core';
import { StoreService } from '../redux/store.service';
import { Logger } from '../redux/logger.middleware';
import { CounterReducer, INCREMENT, DECREMENT } from '../redux/counter.reducer';
import { State } from '../redux/state';
import { Blocker } from '../redux/blocker.middleware';

@Component({
    selector: 'test-counter',
    templateUrl: './counter.component.html',
    styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {

    counter: number;

    constructor(
        private store: StoreService
    ) { }

    ngOnInit(): void {
        this.store.getStore().observableState.subscribe( ( state: State ) => {
            this.counter = state.counter.counter;
        } );
     }

    increment(): void {
        this.store.getStore().dispatch( { type: INCREMENT } ).subscribe();
    }

    decrement(): void {
        this.store.getStore().dispatch( { type: DECREMENT } ).subscribe();
    }
}
