import { State } from './state';
import { Action } from './action';

export interface Middleware {

    apply( store: State, action: Action ): Action;
}