import { Reducer } from './reducer';
import { State } from './state';
import { Action } from './action';
import { CounterState } from './counter.state';

export const INCREMENT = 'INCREMENT';

export const DECREMENT = 'DECREMENT';

export class CounterReducer implements Reducer<CounterState> {

    apply( state: CounterState, action: Action ): CounterState {
        switch( action.type ) {
            
            case INCREMENT:
                return {
                    ...state,
                    counter: state.counter + 1
                };

            case DECREMENT:
                return {
                    ...state,
                    counter: state.counter - 1
                };
            
            default:
                return state;
        }
    }

}