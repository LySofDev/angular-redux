import { Stateful } from './stateful';

export interface CounterState extends Stateful {
    counter: number;
}