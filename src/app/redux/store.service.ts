import { Injectable } from '@angular/core';
import { Store } from './store';
import { CounterReducer } from './counter.reducer';
import { Logger } from './logger.middleware';

@Injectable()
export class StoreService {

    private store: Store;

    getStore(): Store {
        if ( !this.store ) {
            this.store = this.createStore();
        }
        return this.store;
    }

    private createStore(): Store {
        return new Store( {
            middlewares: [
                new Logger()
            ],
            reducers: [
                new CounterReducer()
            ],
            state: {
                counter: {
                    counter: 0
                }
            }
        } );
    }

}