import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Observer } from 'rxjs';
import { State } from './state';
import { Action } from './action';
import { Middleware } from './middleware';
import { Reducer } from './reducer';
import { Stateful } from './stateful';

export class Store {

    private middlewares: Middleware[] = [];

    private reducers: Reducer<any>[] = [];

    private state: BehaviorSubject<State> = new BehaviorSubject<State>( {
        counter: {
            counter: 0
        }
    } );

    get currentState(): State {
        return this.state.getValue();
    }

    get observableState(): Observable<State> {
        return this.state;
    }

    constructor( config: { middlewares: Middleware[], reducers: Reducer<any>[], state: State } ) {
        this.middlewares = config.middlewares;
        this.reducers = config.reducers;
        this.state.next( config.state );
    }

    dispatch( action: Action ): Observable<boolean> {
        return Observable.create( ( observer: Observer<boolean> ) => {
            let middleware: Middleware;
            let reducer: Reducer<any>;
            let mi: number = 0;
            let state: State = this.currentState;
            try {
                while ( action && mi < this.middlewares.length ) {
                    middleware = this.middlewares[mi];
                    action = middleware.apply( state, action );
                    mi++;
                }
                if ( action ) {
                    Object.keys( this.reducers ).forEach( ( identifier: string ) => {
                        reducer = this.reducers[identifier];
                        state = reducer.apply( state, action );
                    } );
                    this.state.next( state );
                    observer.next( true );
                } else {
                    observer.next( false );
                }
            } catch ( err ) {
                observer.error( err );
            }
            observer.complete();
        } );
    }

    registerMiddleware( middleware: Middleware ): void {
        this.middlewares.push( middleware );
        
    }

    registerReducer<T extends Stateful>( reducer: Reducer<T> ): void {
        this.reducers.push( reducer );
    }
    
}