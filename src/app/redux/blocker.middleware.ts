import { Middleware } from './middleware';
import { State } from './state';
import { Action } from './action';

export class Blocker implements Middleware {

    apply( _state: State, _action: Action ): Action {
        return null;
    }

}
