import { State } from './state';
import { Action } from './action';
import { Stateful } from './stateful';

export interface Reducer<T extends Stateful> {

    apply( state: T, action: Action ): T;
    
}