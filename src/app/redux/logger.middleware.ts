import { Middleware } from './middleware';
import { State } from './state';
import { Action } from './action';

export class Logger implements Middleware {
    apply( state: State, action: Action ): Action {
        console.log( 'STATE', state );
        console.log( 'ACTION', action );
        return action;
    }
}